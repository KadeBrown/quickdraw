﻿using UnityEngine;
using System.Collections;

public class Death_Collide : MonoBehaviour
{
    public float deathDelay = 0;
    
    void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject, deathDelay);
    }

    void OnCollisionEnter(Collision other)
    {
        Destroy(gameObject, deathDelay);
    }
}