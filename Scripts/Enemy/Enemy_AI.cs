﻿using UnityEngine;
using System.Collections;
using System.Linq;


public class Enemy_AI : MonoBehaviour
{
    #region Variables
    #region States
    //Finite State Machine
    public State currentState;
    private bool fleeing = false;
    public enum State
    {
        SEEKING,
        IDLE,
        WANDER,
        SHOOTING,
        FLEEING,
    }
    #endregion
    #region Movement Options
    [Header("Enemy Movement Options")]
    public float movementSpeed;             //How fast the enemy moves across the terrain
    public float wanderRadius;
    private Vector3 closestObjectLocation; //Where the navmesh wants to set its destination to ( usually the players position)
    private float wanderTimer;              //Adds a delay before getting a new wander closestObject location
    private NavMeshAgent navMesh;        //The navmesh agent that moves the player around the terrain
    public string barrierTag;
    #endregion
    #region General Settings
    [Header("Test")]
    private float canSeePlayerTimer;        //Counts up to 0.5 to add delay on searching for player
    public float detectionRadius;
    public float shootRadius;
    public bool isBoss;
    public float fleeTimer;
    public float initFleeTimer;
    public AudioSource fleeingSound;
    public GameObject closestObject;
    private GameObject closestObjective;
    public GameObject closestPlayer;
    #endregion
    #region Shooting
    [Header("Enemy Shooting Settings")]
    public float    timeBetweenShots;
    public float    bulletSpread;
    public float    bulletRange;
    public float    reloadSpeed;
    public float    fireSpeed;
    public float    bulletsPerShot;
    public int      bulletDamage;
    private float   recentlyShot;

    public AudioSource gunShootingSound;

    public GameObject raycastOrigin;
    public GameObject bulletOrigin;

    private float shootTimer = 0;
    private float rayCastCounter = 0;
    private float shootingTimer = 2;
    #endregion
    #region Tracers
    [Header("Tracers")]
    //public Rigidbody tracer;
    public GameObject tracerBullet;
    public Transform tracerSpawnPoint;
    [Space(10)]
    public float tracerSpeed;
    #endregion
    #endregion

    #region Functions

    void Start()
    {
        navMesh = gameObject.GetComponent<NavMeshAgent>();
        currentState = State.WANDER;
        navMesh.speed = movementSpeed;
        recentlyShot = timeBetweenShots;
        wanderTimer = 3;
        initFleeTimer = fleeTimer;
    }

    void Update()
    {
        FindClosestObject();
        CheckForChangeState();
        UpdateState();
        navMesh.Resume();
    }

    void UpdateState()
    {
        switch (currentState)
        {
            case State.SEEKING:
                SeekPlayer();
                break;
            case State.IDLE:
                Idle();
                break;
            case State.WANDER:
                Wander();
                break;
            case State.SHOOTING:
                Shooting();
                break;
            case State.FLEEING:
                Flee();
                break;
            default:
                break;
        }
    }

    void CheckForChangeState()
    {
        if (fleeing) return;

        #region Seeking & Shooting
        //If can see player, seekforplayer

        if (Vector3.Distance(transform.position, closestObject.transform.position) < detectionRadius)
        {
            //Raycast to see if you can see player
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, (closestObject.transform.position - transform.position), out hitInfo))
            {
                if (hitInfo.transform.gameObject == closestObject)
                {
                    recentlyShot += Time.deltaTime;
                    if (recentlyShot >= timeBetweenShots)
                    {
                        ChangeState(State.SHOOTING);
                        recentlyShot = 0;
                        Invoke("ChangeStateSeeking", 0.5f);
                    }
                }
                else
                {
                    ChangeState(State.WANDER);
                }
            }
        }
        #endregion
    }

    void ChangeStateSeeking()
    {
        currentState = State.SEEKING;
    }

    void ChangeState(State nextState)
    {
        currentState = nextState;
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    Transform SearchForCover(Transform notInclude)
    {
        GameObject[] closestObjects;
        closestObjects = GameObject.FindGameObjectsWithTag(barrierTag.ToString());
        closestObject = closestObjects[0];
        for (int i = 1; i < closestObjects.Length; i++)
        {
            float first = Vector3.Distance(closestObjects[i].transform.position, transform.position);
            float second = Vector3.Distance(closestObjects[i - 1].gameObject.transform.position, transform.position);
            if (first < second)
            {
                closestObject = closestObjects[i];
            }
        }
        return closestObject.transform;
    }

    #region State Functions
    //State Functions
    void SeekPlayer()
    {
        if (Vector3.Distance(transform.position, closestObject.transform.position) < 8)
        {
            navMesh.Stop();
            //Die!!!
        }
        else
        {
            navMesh.SetDestination(closestObject.transform.position);
            //Play "Where is he" sound
        }
    }

    void Idle()
    {
        navMesh.Stop();
        //Start idle animation
    }

    void Flee()
    {
        if (fleeTimer > 0)
        {
            fleeing = true;
            fleeTimer -= Time.deltaTime;

            if (isBoss)
            {
                if (navMesh.SetDestination(SearchForCover(null).position))
                {
                    //Play flee sound

                    //navMesh.SetDestination(SearchForCover(null).position);
                    if (Vector3.Distance(transform.position, SearchForCover(null).position) < 0.1f)
                    {
                        Vector3 direction = Camera.current.transform.position - raycastOrigin.transform.position;

                        Ray r = new Ray(raycastOrigin.transform.position, direction);
                        RaycastHit hit;

                        if (Physics.Raycast(r, out hit))
                        {
                            Debug.DrawRay(transform.position, direction);

                            if (hit.transform.gameObject.tag == "Body" || hit.transform.gameObject.tag == "Head" || hit.transform.gameObject.tag == "Player")
                            {
                                SearchForCover(closestObject.transform);
                            }
                        }
                    }
                }
            }
            else
            {
                navMesh.SetDestination(RandomNavSphere(transform.position, wanderRadius, 0));
            }
        }
        else
        {
            fleeing = false;
            fleeTimer = initFleeTimer;
            ChangeState(State.WANDER);
        }
    }

    void Wander()
    {
        wanderTimer += Time.deltaTime;

        if (wanderTimer >= 3)
        {
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            navMesh.SetDestination(newPos);
            wanderTimer = 0;
            //Play wandering sound
        }
    }
    
    void Shooting()
    {
        navMesh.SetDestination(transform.position);
        if (gameObject.GetComponent<Enemy_Health>().health >= 0)
        {
            if (rayCastCounter <= 0)
            {
                //checks to see if the enemy can see the player
                rayCastCounter = fireSpeed;

                Vector3 direction = closestObject.transform.position - raycastOrigin.transform.position;
                
                direction += new Vector3(0, 1, 0);
                Ray r = new Ray(raycastOrigin.transform.position, direction);
                RaycastHit hit;

                if (Physics.Raycast(r, out hit))
                {
                    Debug.DrawRay(transform.position, direction);

                    //if the player is seen
                    if (hit.collider.gameObject.tag == "Body" || hit.collider.tag == "Head" || hit.transform.gameObject.tag == "Player" || hit.transform.gameObject.tag == "Interactable")
                    {
                        if(hit.collider.gameObject.name != "Flag")
                            ShootBurst();
                    }
                    else
                    {
                    }
                }
                else
                {
                    //move to new location
                    for (int i = 0; i < 30; ++i)
                        Debug.DrawLine(raycastOrigin.transform.position, hit.point);
                }
            }
            //stops the enemy raycasting every frame to help performance
            if (rayCastCounter > 0)
                rayCastCounter -= Time.deltaTime;
        }
    }
    #endregion

    public void ResetFleeTimer()
    {
        fleeTimer = initFleeTimer;
    }
    
    void ShootRay()
    {
        if (gunShootingSound != null)
            gunShootingSound.Play();

        float randomRadius = Random.Range(0, bulletSpread);                                     // ray casts will be in a circle area
        float randomAngle = Random.Range(0, 2 * Mathf.PI);                                      // ray casts will be in a circle area
        transform.LookAt(closestObject.gameObject.transform.position);                      //look at the player before shooting forwards

        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle),
                            randomRadius * Mathf.Sin(randomAngle), bulletRange);                //Calculate raycast direction

        direction = bulletOrigin.transform.TransformDirection(direction.normalized);            //make direction match the transform
        
        // eg. converting the vector3 forward to transform forward

        Ray r = new Ray(raycastOrigin.transform.position, direction);                           //raycast to the player
        RaycastHit hit;

        if ((transform.position - closestObject.transform.position).magnitude < shootRadius)
        {
            if (Physics.Raycast(r, out hit))
            {
                ShootTracer(direction);
                switch (hit.collider.tag)
                {
                    case "Player":
                        if (hit.collider.transform.root.GetComponent<Player_Health>())
                        {
                            hit.collider.transform.root.GetComponent<Player_Health>().TakeDamage(bulletDamage); 
                        }
                        break;
                    case "Body":
                        if (hit.collider.transform.root.GetComponent<Player_Health>())
                        {
                            hit.collider.transform.root.GetComponent<Player_Health>().TakeDamage(bulletDamage); 
                        }
                        break;
                    case "Head":
                        if (hit.collider.transform.root.GetComponent<Player_Health>())
                        {
                            hit.collider.transform.root.GetComponent<Player_Health>().TakeDamage(bulletDamage);  
                        }
                        break;
                    case "Interacable":
                        {
                            if (hit.collider.name != "Flag")
                            {
                                hit.collider.gameObject.GetComponent<Objective_Generator>().TakeDamage(bulletDamage);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    void ShootTracer(Vector3 direction)
    {
        Vector3 origin = bulletOrigin.transform.position;
        GameObject tracer = Instantiate(tracerBullet, origin + direction, transform.rotation) as GameObject;
        tracer.GetComponent<Rigidbody>().AddForce(direction * tracerSpeed);
        Destroy(tracer, 1);
    }

    void ShootBurst()
    {
        Invoke("ShootRay", 0);
        Invoke("ShootRay", 0.1f);
        Invoke("ShootRay", 0.2f);
    }

    void FindClosestObject()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        GameObject[] list = GameObject.FindGameObjectsWithTag("Interactable");
        var combinedList = players.Concat(list).ToArray();

        float minDist = Mathf.Infinity;

        foreach (GameObject t in combinedList)
        {
            float dist = Vector3.Distance(t.transform.position, transform.position);
            if (dist < minDist && t.name != "Flag")
            {
                closestObject = t;
                minDist = dist;
            }
        }
    }
    #endregion
}
