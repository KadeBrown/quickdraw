﻿using UnityEngine;
using System.Collections;

public class Enemy_Spawn : MonoBehaviour
{
    [Header ("Spawning")]
    private     GameObject      playerTarget;
    public      GameObject[]    enemiesToSpawn;
    public      float           spawnDelay                  = 1.0f;
    public      bool            spawnEnemies;
    public      int             totalPsychos;
    public      int             totalSoldiers;
    public      int             totalCaptains;
    public      int             howManySpawnAtEachTick      = 1;

    private     int             totalEnemies;
    private     int             howManyHaveBeenSpawned;
    private     int             amountOfDifferentEnmies;

    private float               spawnTimer                  = 0;
    private GameObject[]        players;
        void Start()
    {
        spawnTimer = spawnDelay;
        totalEnemies = totalCaptains + totalPsychos + totalSoldiers;
    }

    void Update()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            if (Vector3.Distance(players[0].transform.position, transform.position) < Vector3.Distance(players[1].transform.position, transform.position))
            {
                playerTarget = players[0];
            }
            else
            {
                playerTarget = players[1];
            }
        }

        if (players.Length == 1)
        {
            playerTarget = players[0].gameObject;
        }

        if (spawnEnemies)
        {
            if (howManyHaveBeenSpawned < totalEnemies)
            {
                spawnTimer -= Time.deltaTime;
                if (spawnTimer <= 0)
                {
                    SpawnEnemy(totalPsychos,    0);
                    SpawnEnemy(totalSoldiers,   1);
                    SpawnEnemy(totalCaptains,   2);
                    spawnTimer = spawnDelay;
                }
            }
        }
    }

    void SpawnEnemy(int spawnCount, int type)
    {
        for (int i = 0; i < spawnCount; ++i)
        {
            GameObject enemy = Instantiate(enemiesToSpawn[type], transform.position, transform.rotation) as GameObject;
            enemy.transform.position += new Vector3(Random.Range(0, 10), Random.Range(0, 10), Random.Range(0, 10));
            enemy.GetComponent<Enemy_Base>().closest_target = playerTarget;
            enemy.transform.parent = GameObject.Find("EnemiesHolder").transform;
            howManyHaveBeenSpawned++;
        }
    }
}
