﻿using UnityEngine;
using System.Collections;

public class Item_Drop : MonoBehaviour
{
    private Vector3 tempPos;

    public int pickUpValue = 0; //What ever this is equal to will make the player pickup that much ie: pickUpValue = 100, and tagged ammo, player will pick up 100 ammo etc

    // Update is called once per frame
    void Update()
    {
        if (Time.deltaTime > 0)
        {
            transform.Rotate(new Vector3(0, 2, 0));
        }

        tempPos = transform.position;
        tempPos.y = 0.6f + 0.3f * Mathf.Sin(1 * Time.time);

        transform.position = tempPos;
    }

    void OnTriggerEnter(Collider other)
    {
        if (gameObject.tag == "Health")
        {
            if (other.gameObject.tag == "Player" || other.gameObject.tag == "Body")
            {
                other.transform.root.GetComponent<Player_Health>().AddHealth(pickUpValue);
                Destroy(gameObject);
            }
        }
        else if (gameObject.tag == "headshotPickup")
        {
            if (other.gameObject.tag == "Player" || other.gameObject.tag == "Body")
            {
                other.transform.root.GetComponent<Player_NetworkManager>().CmdUpdateBuffs(other.transform.root.GetComponent<Player_NetworkManager>().netId, 9, 0);
                Destroy(gameObject);
            }
        }

    }
}
