﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class GIF_Script : MonoBehaviour
{

    public bool looping = false;
    public bool play = false;
    public bool isImage = false;
    public Sprite[] textures;
    public float imageDelay = 0.5f;
    public bool destroyOnEnd = false;
    public GameObject muzzleLight;

    private float delayCounter = 0;
    private int textureCounter = 0;

    // Use this for initialization
    void Start()
    {
        delayCounter = imageDelay;
    }

    // Update is called once per frame
    void Update()
    {

        if (play)
            Play();
        else
        {
               textureCounter = 0;
            if (muzzleLight != null)
                muzzleLight.GetComponent<Light>().enabled = false;
            if (!isImage)
                GetComponent<SpriteRenderer>().sprite = null;
            else
                GetComponent<Image>().sprite = textures[textureCounter];
        }


    }

    void Play()
    {
        delayCounter -= Time.deltaTime;

        if (textureCounter < 0 && !looping)
        {
            play = false;
            if (destroyOnEnd)
                Destroy(gameObject);
            return;
        }

        if (delayCounter <= 0)
        {
            textureCounter = (textureCounter < textures.Length) ? textureCounter + 1 : -1;
            delayCounter = imageDelay;
        }

        if (textureCounter <= textures.Length - 1 && textureCounter >= 0)
        {
            if (muzzleLight != null)
                muzzleLight.GetComponent<Light>().enabled = true;
            if (!isImage)
                GetComponent<SpriteRenderer>().sprite = textures[textureCounter];
            else
                GetComponent<Image>().sprite = textures[textureCounter];
        }

    }
}
