﻿using UnityEngine;
using System.Collections;

public class Gate_Opener : MonoBehaviour
{
    public float delay;
    private Animator anim;


    void Start()
    {
        anim.GetComponent<Animator>();
    }

    void Update ()
    {
        delay -= Time.deltaTime;
        if (delay <= 0)
        {
            anim.Play("Lower");
        }
	}
}
