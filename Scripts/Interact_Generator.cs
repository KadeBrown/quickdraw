﻿using UnityEngine;
using System.Collections;

public class Interact_Generator : Base_Interact {

	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Interact()
    {
        //base.Interact();

        interacted = true;
        if (GetComponent<Objective_Generator>().active == false)
        {
            GetComponent<Objective_Generator>().active = true;
            return;
        }
    }
}
