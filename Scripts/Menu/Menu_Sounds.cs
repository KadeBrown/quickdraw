﻿using UnityEngine;
using System.Collections;

public class Menu_Sounds : MonoBehaviour
{


    public AudioSource buttonClick;
    public AudioSource hoverButtonSound;
	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    public void PlayButtonSound()
    {
        if (buttonClick != null)
            buttonClick.Play();
    }
}
