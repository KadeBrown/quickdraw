﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Objective_Generator : MonoBehaviour
{
    #region Variables
    #region Buff Settings
    public float speedProbability   = 33;
    public float healthProbability  = 33;
    public float damageProbability  = 34;
    public List<BuffProb> probList;

    public class BuffProb
    {
        public float probability;
        public float cumulativeProb;
        public float buffValue;
        public int buffID;
    }
    #endregion
    #region Others
    public bool active              = false;
    public float activeTime         = 10.0f;
    public int totalHealth          = 100;
    public int currentHealth        = 0;
    [HideInInspector]
    public GameObject player        = null;
    [HideInInspector]
    public GameObject enemyPlayer   = null;

    public bool buffSelected        = false;
    public int buffID;
    public float buffValue;

    private float activeTimer       = 0;
    private Canvas canvas           = null;
    private Text timerText          = null;
    private bool objectiveComplete  = false;

    #endregion
    #endregion

    void Start()
    {
        activeTimer = activeTime;
        currentHealth = totalHealth;

        #region Buff Settings
        probList = new List<BuffProb>();    //Instantiates new list
        
        BuffProb speed = new BuffProb();   //Making new speed value
        BuffProb health = new BuffProb();   //Making new health value
        BuffProb damage = new BuffProb();   //Making new damage value

        // Movement
        speed.cumulativeProb    = 0;
        speed.probability       = speedProbability;
        speed.buffID            = 1;

        // Health
        health.cumulativeProb   = 0;
        health.probability      = healthProbability;
        health.buffID           = 4;

        // Damage
        damage.cumulativeProb   = 0;
        damage.probability      = damageProbability;
        damage.buffID           = 3;

        probList.Add(health);
        probList.Add(speed);
        probList.Add(damage);
        #endregion

        RandomBuff();
    }

    // Update is called once per frame
    void Update()
    {
        if (active && !objectiveComplete)
        {
            //gameObject.GetComponent<Material>().color = Color.red;
            if (player == null)
                FindLocalPlayer();

            if (canvas == null)
                canvas = player.GetComponent<Player_Health>().canvasInstance;

            if (canvas != null && timerText == null)
            {
                timerText = canvas.transform.Find("ObjectiveText").GetComponent<Text>();

                if (!timerText.enabled)
                    timerText.enabled = true;
            }

            if (currentHealth > 0)
            {
                activeTimer -= Time.deltaTime;
                timerText.text = "GENERATOR STARTING IN: " + ((int)activeTimer).ToString();
                if (activeTimer <= 0)
                {
                    //reward player with buffs
                    objectiveComplete = true;
                }
            }
            else
            {
                //destroy objective
                active = false;
            }
        }
        else if (objectiveComplete && !buffSelected)
        {
            timerText.enabled = false;
            FindLocalPlayer();
            player.GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
            buffSelected = true;
            //gameObject.GetComponent<Material>().color = Color.blue;
        }        
    }

    void RandomBuff()
    {
        probList[0].cumulativeProb = probList[0].probability;
        int randomNumber = Random.Range(0, 100);

        if (probList[0].cumulativeProb > randomNumber)
        {
            buffID = probList[0].buffID;
            buffValue = probList[0].buffValue;
            return;
        }

        for (int i = 1; i < probList.Count; ++i)
        {
            probList[i].cumulativeProb = probList[i].probability + probList[i - 1].cumulativeProb;

            if (probList[i].cumulativeProb > randomNumber)
            {
                buffID = probList[i].buffID;
                buffValue = probList[i].buffValue;
                return;
            }
        }
    }

    public void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
                enemyPlayer = players[1];
            }
            else
            {
                player = players[1];
                enemyPlayer = players[0];
            }
        }
        else if (players.Length == 1)
            player = players[0];

    }
    
    public void TakeDamage(int dmg)
    {
        currentHealth -= dmg;
    }
}
