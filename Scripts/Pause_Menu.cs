﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Pause_Menu : MonoBehaviour
{
    public  Canvas pauseMenu;
    private Camera mainCamera;

	// Use this for initialization
	void Start ()
    {
        Camera[] cameras = GameObject.FindObjectsOfType<Camera>();
        for (int i = 0; i < cameras.Length; ++i)
        {
            if (cameras[i].enabled == true && cameras[i].tag == "PlayerCamera")
                mainCamera = cameras[i];
        }
    }
	
	void Update ()
    {
        //Activate Pause Menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //mainCamera.GetComponent<BlurOptimized>().enabled = true;
            pauseMenu.enabled = !pauseMenu.enabled;
        }        
	}
}
