﻿using UnityEngine;
using System.Collections;

public class Hax0r : MonoBehaviour
{
    private bool healthHAX;
    private bool GODMODE;

    private int initdmg;
    private float initmoveSpeed;
    private float initjumpForce;
    private float initfireSpeed;
    // Use this for initialization
    void Start()
    {
        initdmg = GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().bulletDamage;
        initmoveSpeed = GetComponent<Player_Movement>().initMoveSpeed;
        initjumpForce = GetComponent<Player_Movement>().jumpForce;
        initfireSpeed = GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().fireSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        if (healthHAX)
        {
            GetComponent<Player_Health>().currentHealth = 100;
        }

        if (GODMODE)
        {
            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().bulletDamage = 10000;
            healthHAX = true;
            GetComponent<Player_Movement>().initMoveSpeed = 400;
            GetComponent<Player_Movement>().jumpForce = 100;
            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().fireSpeed = 0.001f;
            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().bulletsInMag = 999;
        }
        else
        {
            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().bulletDamage = initdmg;
            GetComponent<Player_Movement>().initMoveSpeed = initmoveSpeed;
            GetComponent<Player_Movement>().jumpForce = initjumpForce;
            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().fireSpeed = initfireSpeed;
        }
    }

    void GetInput()
    {
        #region Buffers and DeBufffers
        //Damage
        if (Input.GetKey(KeyCode.Alpha1))
        {
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().bulletDamage += 100;

            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().bulletDamage -= 100;
            }
        }

        //Health
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            healthHAX = !healthHAX;
        }

        //Movement
        if (Input.GetKey(KeyCode.Alpha3))
        {
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                GetComponent<Player_Movement>().initMoveSpeed *= 10;
            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                GetComponent<Player_Movement>().initMoveSpeed /= 10;
            }
        }

        //Jump Height
        if (Input.GetKey(KeyCode.Alpha4))
        {
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                GetComponent<Player_Movement>().jumpForce *= 10;
            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                GetComponent<Player_Movement>().jumpForce *= 10;
            }
        }

        //Fire Speed
        if (Input.GetKey(KeyCode.Alpha5))
        {
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().fireSpeed *= 10;
            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().fireSpeed /= 10;
            }
        }
        #endregion
        #region GODMODE
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            GODMODE = !GODMODE;
        }
        #endregion
    }
}
