﻿using UnityEngine;
using System.Collections;

public class Player_Movement : MonoBehaviour
{
    #region Variables
    [Header("Player Movement")]
    public float moveSpeed = 10;                          // Rate at which the player moves
    public float initMoveSpeed;                                                     // Rate at which the player moves
    public float runSpeed = 1.5f;                  // Multiplier that alters move speed when running
    public float drag;
    public float maxVelocity;
    public float jumpForce = 1.0f;                      // How high the player jumps
    public float groundCheckDistance = 0.001f;          // Distance used for checking if player is grounded (0.01 works best)
    public float stickToGroundDistance = 0.5f;          // Stops the player
    public float capsuleOffset = 0.01f;                 // Reduces capsule radius by value to avoid getting stuck in walls
    public KeyCode runKey = KeyCode.LeftShift;          // Key press to start running
    public bool active = true;

    private bool isRunning = false;                // Bool used to check if the player is running
    private bool jumping = false;                // Bool used to determine whether the player is jumping
    private bool jump = false;                // Bool used to check whether the player should jump
    private bool addedForce = false;                // Bool used to prevent double jumps
    private bool previouslyGrounded = false;                // Bool used to check if the player was grounded last frame
    private float currentSpeed = 0.0f;                 // used to move the player
    private Rigidbody rigidbody = null;                 // Player's rigidbody (used for moving)
    private CapsuleCollider capsule = null;                 // Player's collider (used for collision detection)
    private Vector3 groundContactNormal = Vector3.zero;         // Used to determine correct action when jumping
    private bool sprinting;
    private Animator animator;
    private Gun_Script activeGun;
    public bool isGrounded = true;                     // Bool used to check if the player is in the air
    public bool ADSSlow;
    #endregion

    #region Functions
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        capsule = transform.GetChild(0).GetComponent<CapsuleCollider>();
        currentSpeed = moveSpeed;
        initMoveSpeed = moveSpeed;
    }

    void FixedUpdate()
    {
        if (active)
        {
            animator = transform.root.GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().playerAnimator;
            GetInput();
        }

    }

    void GetInput()
    {
        GroundCheck();

        #region Movement
        if (Input.GetKey(KeyCode.W))
        {
            rigidbody.AddForce(transform.forward * Time.deltaTime * moveSpeed, ForceMode.Impulse);
            GetComponent<Player_NetworkManager>().CmdSyncAnimation(GetComponent<Player_NetworkManager>().netId, "Walk", true);
        }

        if (Input.GetKey(KeyCode.A))
        {
            rigidbody.AddForce(-transform.right * Time.deltaTime * moveSpeed, ForceMode.Impulse);
            GetComponent<Player_NetworkManager>().CmdSyncAnimation(GetComponent<Player_NetworkManager>().netId, "Walk", true);
        }

        if (Input.GetKey(KeyCode.S))
        {
            rigidbody.AddForce(-transform.forward * Time.deltaTime * moveSpeed, ForceMode.Impulse);
            GetComponent<Player_NetworkManager>().CmdSyncAnimation(GetComponent<Player_NetworkManager>().netId, "Walk", true);
        }

        if (Input.GetKey(KeyCode.D))
        {
            rigidbody.AddForce(transform.right * Time.deltaTime * moveSpeed, ForceMode.Impulse);
            GetComponent<Player_NetworkManager>().CmdSyncAnimation(GetComponent<Player_NetworkManager>().netId, "Walk", true);
        }

        if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.D))
        {
            GetComponent<Player_NetworkManager>().CmdSyncAnimation(GetComponent<Player_NetworkManager>().netId, "Walk", false);
        }

        rigidbody.AddForce(new Vector3(-rigidbody.velocity.x, 0, -rigidbody.velocity.z) * drag);

        if (isRunning)
        {
            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().canShoot = false;
            GetComponent<Player_NetworkManager>().CmdSyncAnimation(GetComponent<Player_NetworkManager>().netId, "Run", isRunning);
        }
        else
        {
            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().canShoot = true;
            GetComponent<Player_NetworkManager>().CmdSyncAnimation(GetComponent<Player_NetworkManager>().netId, "Run", isRunning);
        }


        if (ADSSlow)
            moveSpeed = 0;
        else
            moveSpeed = initMoveSpeed;
        #endregion

        #region Jumping
        if (Input.GetKey(KeyCode.Space))
            jump = true;

        if (isGrounded)
        {
            rigidbody.drag = 1.0f;
            if (jump)
            {
                rigidbody.drag = 1.0f;
                rigidbody.velocity = new Vector3(rigidbody.velocity.x, 0.0f, rigidbody.velocity.z);
                rigidbody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                addedForce = true;
                jumping = true;
            }

            // if (!jumping && Mathf.Abs(x) < float.Epsilon && Mathf.Abs(z) < float.Epsilon && rigidbody.velocity.magnitude < 1)
            //     rigidbody.Sleep();
        }
        else
        {
            rigidbody.drag = 1.0f;
            rigidbody.AddForce(Vector3.down * 50);
            if (previouslyGrounded && !jumping)
            {
                StickToGround();
            }
        }

        jump = false;
        #endregion

        #region Running
        if (Input.GetKey(runKey))
        {
            if (Input.GetKey(KeyCode.W))
            {
                moveSpeed = initMoveSpeed * runSpeed;
                isRunning = true;
            }
            else
            {
                moveSpeed = initMoveSpeed;
                isRunning = false;
            }
        }
        else
        {
            moveSpeed = initMoveSpeed;
            isRunning = false;
        }

        animator.SetBool("isRunning", isRunning);
        #endregion
    }

    void StickToGround()
    {
        RaycastHit hit;
        if (Physics.SphereCast(transform.position, capsule.radius * (1.0f - capsuleOffset), Vector3.down, out hit,
           ((capsule.height / 2.0f) - capsule.radius) + stickToGroundDistance, 0, QueryTriggerInteraction.Ignore))
        {
            if (Mathf.Abs(Vector3.Angle(hit.normal, Vector3.up)) < 85)
            {
                rigidbody.velocity = Vector3.ProjectOnPlane(rigidbody.velocity, hit.normal);
            }
        }
    }

    void GroundCheck()
    {
        previouslyGrounded = isGrounded;

        RaycastHit hit;

        //raycast down to see if ground is there
        if (Physics.SphereCast(transform.position, capsule.radius * (1.0f - capsuleOffset), Vector3.down, out hit,
            ((capsule.height) - capsule.radius) + groundCheckDistance, ~0, QueryTriggerInteraction.Ignore))
        {
            //if ground is there, set appropriate bools to true
            isGrounded = true;
            groundContactNormal = hit.normal;
        }
        else
        {
            //otherwise set the appropriate bools to false
            isGrounded = false;
            groundContactNormal = Vector3.up;
        }


        if (!previouslyGrounded && isGrounded && jumping)
            jumping = false;

        addedForce = false;
    }

    public void SlowDown()
    {
        ADSSlow = true;
    }

    public void SpeedUp()
    {
        ADSSlow = false;
    }
    #endregion
}
