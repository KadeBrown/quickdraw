﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Player_ShootManager : MonoBehaviour 
{
    public GameObject activeWeapon = null;      // The gun that the player is currently using
    public GameObject primaryWeapon = null;     // The players main weapon
    public GameObject secondaryWeapon = null;   // The players back-up weapon
    public GameObject weaponPosition = null;
    public float interactDelay = 0.5f;          // How long the player will need to hold down the interact button
    public KeyCode interactKey = KeyCode.F; 
    
    private bool UISetup = false;
    private float interactTimer;
    
    [Header("UI")]
    public Text bulletsText;   
    public Image outerReticle;
    public Image hitMarker;
    private Canvas canvas;
    private Vector3 outerReticleScaleInit;
    private int test;

    public GameObject rifleAnims;
    public GameObject quadAnims;

    public AudioSource weaponSwitchSound;

    // Use this for initialization
    void Start ()
    {
        interactTimer = interactDelay;

        activeWeapon = primaryWeapon;

        canvas = GetComponent<Player_Health>().canvasInstance;
        
        rifleAnims.SetActive(true);
        quadAnims.SetActive(false);
    }

    void SetupUI()
    {        
        if (canvas == null)
            canvas = GetComponent<Player_Health>().canvasInstance;
            
        if (canvas != null)
        {
            if (bulletsText == null)
            {
                bulletsText = canvas.transform.FindChild("bulletsText").GetComponent<Text>();
                bulletsText.text = "0";
            }

            if (hitMarker == null)
            {
                hitMarker = canvas.transform.FindChild("HitMarker").GetComponent<Image>();
            }

            if (outerReticle == null)
            {
                outerReticle = canvas.transform.FindChild("outerReticle").GetComponent<Image>();
                outerReticleScaleInit = outerReticle.transform.localScale;
                activeWeapon.GetComponent<Gun_Script>().SetReticle(outerReticle);
            }

            UISetup = true;
        }
        else
        {
            UISetup = false;
            Debug.Log("Canvas not found");
        }
    }

    public void WeaponPickup(GameObject weapon)
    {
        activeWeapon.GetComponent<Gun_Script>().active = false;
    }

    void WeaponSwitch()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && activeWeapon == secondaryWeapon)
        {
            weaponSwitchSound.Play();
            activeWeapon.GetComponent<Gun_Script>().active = false;
            activeWeapon = primaryWeapon;
            activeWeapon.GetComponent<Gun_Script>().playerAnimator = rifleAnims.GetComponent<Animator>();
            rifleAnims.SetActive(true);
            quadAnims.SetActive(false);
            
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) && activeWeapon == primaryWeapon)
        {
            weaponSwitchSound.Play();
            activeWeapon.GetComponent<Gun_Script>().active = false;
            activeWeapon = secondaryWeapon;
            activeWeapon.GetComponent<Gun_Script>().playerAnimator = quadAnims.GetComponent<Animator>();
            rifleAnims.SetActive(false);
            quadAnims.SetActive(true);
        }
    }

    void UpdateUI()
    {
        bulletsText.text = activeWeapon.GetComponent<Gun_Script>().bulletsInMag.ToString() + "/" + activeWeapon.GetComponent<Gun_Script>().ammoCount.ToString();

        //Clamping the outer rings of reticle
        outerReticle.transform.localScale = new Vector3(
        Mathf.Clamp(outerReticle.transform.localScale.x, outerReticleScaleInit.x, outerReticleScaleInit.x * 1.3f),
        Mathf.Clamp(outerReticle.transform.localScale.y, outerReticleScaleInit.y, outerReticleScaleInit.y * 1.3f),
        Mathf.Clamp(outerReticle.transform.localScale.z, outerReticleScaleInit.z, outerReticleScaleInit.z * 1.3f));

        outerReticle.transform.localScale *= 0.99f;
    }
    
    void Update()
    {
        if (GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            if (activeWeapon == null) return;

            if (UISetup == false)
                SetupUI();
            else
                UpdateUI();

            if (activeWeapon.GetComponent<Gun_Script>().active == false)
            {
                activeWeapon.GetComponent<Gun_Script>().active = true;
                activeWeapon.GetComponent<Gun_Script>().SetReticle(outerReticle);
                activeWeapon.GetComponent<Gun_Script>().SetLocalPlayer(GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer());
                activeWeapon.GetComponent<Gun_Script>().SetHitMarker(hitMarker);
            }

        
            //Interact
            if (Input.GetKey(interactKey))
                interactTimer -= Time.deltaTime;
            else
                interactTimer = interactDelay;

            if (interactTimer <= 0)
            {

            }
            WeaponSwitch();
        }
    }
}
