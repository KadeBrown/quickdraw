﻿using UnityEngine;
using System.Collections;

public class RayCastUpUI : MonoBehaviour
{
    bool moving;
    void Start ()
    {
        moving = true;
	}
	
	void Update()
    {
        if (moving)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 30));
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        //if (other.gameObject.tag == "SoftCore")
        {
            moving = false;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        //if (other.gameObject.tag == "SoftCore")
        {
            moving = true;
        }
    }
}
