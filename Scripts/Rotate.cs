﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
    public float xDirection = 0;
    public float yDirection = 0;
    public float zDirection = 0;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        transform.Rotate(new Vector3(xDirection, yDirection, zDirection));
	}
}
