﻿using UnityEngine;
using System.Collections;

public class ShowPingForPlayers : MonoBehaviour
{
    void OnGUI()
    {
        GUILayout.Label("Player ping values");
        int i = 0;
        while (i < Network.connections.Length)
        {
            GUILayout.Label("Player " + Network.connections[i] + " - " + Network.GetAveragePing(Network.connections[i]) + " ms");
            i++;
        }
    }
}
