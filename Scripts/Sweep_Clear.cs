﻿using UnityEngine;
using System.Collections.Generic;

public class Sweep_Clear : MonoBehaviour
{
    [Header("Spawning")]
    private GameObject      playerTarget;
    private GameObject      enemyPlayer;
    private GameObject[]    players;
    public  GameObject[]    enemiesToSpawn;
    [HideInInspector]
    public  List<GameObject> enemiesSpawned;
    public int amountOfEnemiesActive;

    public  float           spawnDelay = 1.0f;
    public  bool            spawnEnemies;
    public  int             totalElement1Enemies;
    public  int             totalElement2Enemies;
    public  int             totalElement3Enemies;
    public  int             howManySpawnAtEachTick = 1;

    private int             totalEnemies;
    private int             howManyHaveBeenSpawned;
    private int             amountOfDifferentEnmies;

    private float           spawnTimer = 0;
    private Canvas          canvas;
    private float           delay;

    #region Buffs  
    public float movementBuffVal = 3.0f;
    public float healthBuffVal = 30.0f;
    public float damageBuffVal = 30.0f;
    public float speedProbability = 50;
    public float healthProbability = 50;
    public float damageProbability = 0;

    private bool buffSelected;
    private int buffID;
    private float buffValue;
    public List<BuffProb> probList;

    public class BuffProb
    {
        public float probability;
        public float cumulativeProb;
        public float buffValue;
        public int buffID;
    }
    #endregion

    void Start()
    {
        spawnTimer = spawnDelay;
        totalEnemies = totalElement1Enemies + totalElement2Enemies + totalElement3Enemies;
        enemiesSpawned = new List<GameObject>();

        #region Buff Settings And Startup
        probList = new List<BuffProb>();    //Instantiates new list

        BuffProb speed = new BuffProb();   //Making new speed value
        BuffProb health = new BuffProb();   //Making new health value
        BuffProb damage = new BuffProb();   //Making new health value

        // Movement
        speed.cumulativeProb = 0;
        speed.probability = speedProbability;
        speed.buffID = 1;
        speed.buffValue = movementBuffVal;

        // Damage
        damage.cumulativeProb = 0;
        damage.probability = damageProbability;
        damage.buffID = 3;
        damage.buffValue = damageBuffVal;

        // Health
        health.cumulativeProb = 0;
        health.probability = healthProbability;
        health.buffID = 4;
        health.buffValue = healthBuffVal;


        probList.Add(health);
        probList.Add(speed);
        probList.Add(damage);
        #endregion
        RandomBuff();
    }

    void Update()
    {
        if (playerTarget == null)
            FindLocalPlayer();
        SpawnEnemies();

        if (canvas == null && playerTarget != null)
            canvas = playerTarget.GetComponent<Player_Health>().canvasInstance;
        amountOfEnemiesActive = enemiesSpawned.Count;

        CheckIfCompleted();
    }

    void SpawnEnemies()
    {
        if (spawnEnemies)
        {
            if (howManyHaveBeenSpawned < totalEnemies)
            {
                spawnTimer -= Time.deltaTime;
                if (spawnTimer <= 0)
                {
                    SpawnEnemy(totalElement1Enemies, 0);
                    SpawnEnemy(totalElement2Enemies, 1);
                    SpawnEnemy(totalElement3Enemies, 2);
                    spawnTimer = spawnDelay;
                }
            }
        }
    }

    void CheckIfCompleted()
    {
        delay += Time.deltaTime;
        if (delay > 2)
        {
            if (enemiesSpawned.Count == 0)
            {
                if (!buffSelected)
                {
                    FindLocalPlayer();
                    playerTarget.GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
                    buffSelected = true;
                    //Add my Buff ID to list of buffIDs in buff manager
                }
            }
        }
    }

    void SpawnEnemy(int spawnCount, int type)
    {
        for (int i = 0; i < spawnCount; ++i)
        {
            GameObject enemy = Instantiate(enemiesToSpawn[type], transform.position, transform.rotation) as GameObject;
            enemy.transform.position += new Vector3(Random.Range(0, 10), Random.Range(0, 10), Random.Range(0, 10));
            enemy.GetComponent<Enemy_Base>().closest_target = playerTarget;
            enemy.transform.parent = this.gameObject.transform;
            enemy.GetComponent<Enemy_Base>().sweep_clear_flag = true;
            enemiesSpawned.Add(enemy);
            howManyHaveBeenSpawned++;
        }
    }
    
    void RandomBuff()
    {
        probList[0].cumulativeProb = probList[0].probability;
        int randomNumber = Random.Range(0, 100);

        if (probList[0].cumulativeProb > randomNumber)
        {
            buffID = probList[0].buffID;
            buffValue = probList[0].buffValue;
            return;
        }

        for (int i = 1; i < probList.Count; ++i)
        {
            probList[i].cumulativeProb = probList[i].probability + probList[i - 1].cumulativeProb;

            if (probList[i].cumulativeProb > randomNumber)
            {
                buffID = probList[i].buffID;
                buffValue = probList[i].buffValue;
                return;
            }
        }
    }

    void DeSpawn()
    {
        Destroy(gameObject, 1);
    }
    
    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                playerTarget = players[0];
                enemyPlayer = players[1];
            }
            else
            {
                playerTarget = players[1];
                enemyPlayer = players[0];
            }
        }
        else if (players.Length == 1)
            playerTarget = players[0];


    }

    public void TakeEnemyOutOfList(GameObject enemy)
    {
        enemiesSpawned.Remove(enemy);
    }
}
