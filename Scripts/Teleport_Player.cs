﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Teleport_Player : MonoBehaviour
{
    public float teleportTime;
    public GameObject teleportTarget;
    public GameObject objToLookAt = null;
    private GameObject player = null;
    private GameObject enemy = null;
    private bool teleported = false;
    public Text countDownText;
    public Text gameplayText;

    private string minutes;
    private string seconds;


    void Update()
    {
        if (Debug.isDebugBuild)
        {
            if (Input.GetKeyDown(KeyCode.T))
                teleportTime = 0;
        }
        if (!teleported)
        {
            minutes = Mathf.Floor(teleportTime / 60).ToString("00");
            seconds = (teleportTime % 60).ToString("00");
            if (seconds == "60")
            {
                seconds = "59";
                teleportTime -= Time.deltaTime;
            }
            gameplayText.GetComponent<Text>().text = minutes + ":" + seconds;
        }
    }

    void FixedUpdate()
    {
        if (!teleported)
        {
            FindLocalPlayer();

            teleportTime -= Time.deltaTime;
            if (teleportTime <= 0)
            {
                if (player != null && enemy != null && player != enemy)
                {
                    player.transform.root.GetComponent<Player_NetworkManager>().CmdTeleportPlayers(player.GetComponent<Player_NetworkManager>().netId,
                                        enemy.GetComponent<Player_NetworkManager>().netId, this.transform.position, teleportTarget.transform.position, objToLookAt.transform.position);


                    teleported = true;
                    countDownText.enabled = false;
                }
                else
                {
                    teleported = true;
                }
            }




            if (teleportTime <= 10)
            {
                if (!teleported)
                {
                    //countDownText.text = "You will be teleported in " + (int)teleportTime + " seconds";
                    gameplayText.text = "You will be teleported in " + (int)teleportTime + " seconds";
                    //gameplayText.gameObject.SetActive(false);
                }
                else
                {
                    //gameplayText.gameObject.SetActive(false);
                    gameplayText.text = "FIGHT!";
                }
            }

        }
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
                enemy = players[1];
            }
            else
            {
                player = players[1];
                enemy = players[0];
            }
        }
        else if (players.Length == 1)
            player = players[0];
    }
}
