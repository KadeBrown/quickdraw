﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Gun_Script : MonoBehaviour
{
    #region Variables
    #region Bullet Settings
    [Header("Bullet Settings")]
    protected int initBulletDamage;
    protected float initbulletSpread;   //How large the cone of fire is
    public float bulletSpread;          //How large the cone of fire is
    public float playerHitMultiplier;   //How much depreciated dmg you do to another player
    public float ADS_Spread;            //How large the cone of fire is
    public float bulletRange;           //How far each bullet can go 
    public float reloadSpeed;           //How long it takes to reload 
    public float bulletsPerShot;        //How many bullets were shot at once
    public float fireSpeed;             //Time between shots

    public int bulletDamage;            //How much damage each bullet does on impact
    public int magSize;                 //The max amount of bullets in a magazine
    public int maxAmmo;                 //The max amount of bullets the player can carry at any given time


    protected bool reloading;           //Keeping track if reloading or not
    public int bulletsInMag;            //Tracks the bullets in the current magazine 
    public int ammoCount;               //Tracks the total amount of bullets the player is carrying

    protected float shootTimer;         //Time it takes to shoot
    protected float reloadTimer;        //Tracks how long it takes to reload

    protected bool speedKillStarted;
    protected float speedKillTimer;
    protected int speedKillAttemptsCurrent;

    protected float speedKillTimeLimit;
    protected int speedKillAttemptsTotal;
    #endregion
    #region BulletHole
    [Header("BulletHole")]
    public Sprite[] bulletHoles;        //What the bullet holes look like
    public GameObject bulletHoleObject;   //When a static object is shot this image is instantiated at the point where it hits
    public ParticleSystem wallHitEffect;        //Spawns the particle system where it hits the wall
    public ParticleSystem enemyHitEffect;        //Spawns the particle system where it hits the wall
    public GameObject sparks = null;
    #endregion
    #region Tracers
    [Header("Tracers")]
    public GameObject tracerObject;             //The Tracers rigidBody that will get forced from bulletOrigin
    public GameObject bulletOrigin;       //Where the bullets come from ( Usually tip of gun)
    public float tracerSpeed;        //How fast the tracers move in worldspace after fired
    #endregion
    #region Misc Variables
    [HideInInspector]
    public bool active;
    public bool canShoot = true;
    protected Camera activePlayerCamera;
    protected Image outerReticle = null;
    public bool isLocalPlayer = false;

    public Animator playerAnimator;
    public Animator gunAnimator;

    protected Image passedInHitmarker;
    protected float hitmarkerTimer;
    public Material activeMaterial = null;
    public Material defaultMaterial = null;
    public Material healthMaterial = null;
    public Material speedMaterial = null;
    protected bool materialInvokeCalled = false;
    protected float RimPower;
    protected bool startedReloading;

    public int kills;
    #endregion
    #region AudioSource
    [Header("Audio")]
    public AudioSource fireSound;
    public AudioSource reloadSound;
    public AudioSource adsSound;
    public AudioSource bodyHitSound;
    #endregion
    #endregion

    #region Functions
    // Use this for initialization
    virtual public void Start()
    {
        reloading = false;
        activePlayerCamera = FindActiveCamera();
        active = false;
        bulletsInMag = magSize;
        ammoCount = maxAmmo;
        reloadTimer = reloadSpeed;
        initBulletDamage = bulletDamage;
        startedReloading = false;
        //GetComponent<MeshRenderer>().enabled = false;

        initbulletSpread = bulletSpread;
        activeMaterial = defaultMaterial;
        gunAnimator = transform.GetComponent<Animator>();

        speedKillTimeLimit = transform.root.GetComponent<Softcore_Objectives>().speedKillTimeLimit;
        speedKillAttemptsTotal = transform.root.GetComponent<Softcore_Objectives>().speedKillAttemptsTotal;

    }

    public void SetLocalPlayer(bool isLocal)
    {
        isLocalPlayer = isLocal;
    }

    public void SetReticle(Image reticle)
    {
        outerReticle = reticle;
    }

    public void SetHitMarker(Image hitMarker)
    {
        passedInHitmarker = hitMarker;
    }

    virtual public void Update() { }

    virtual public void FixedUpdate()
    {
        activePlayerCamera = FindActiveCamera();
    }

    virtual public void Shoot() { }

    public void ShootTracer(Vector3 position, Vector3 direction, Quaternion rotation, float force)
    {
        if (transform.root.GetComponent<Player_NetworkManager>().isLocalPlayer)
        {
            if (tracerObject != null)
            {
                GameObject tracer = Instantiate(tracerObject, position, rotation) as GameObject;
                if (tracer != null)
                {
                    tracer.GetComponent<Rigidbody>().AddForce(direction * force);
                    Destroy(tracer, 2);
                }
            }
        }
    }

    public void PlaySound()
    {
        GameObject newAudioSound = new GameObject();
        newAudioSound.AddComponent<AudioSource>();
        newAudioSound.GetComponent<AudioSource>().clip = fireSound.clip;
        newAudioSound.GetComponent<AudioSource>().playOnAwake = false;
        newAudioSound.GetComponent<AudioSource>().Play();

        newAudioSound.transform.parent = transform;
        newAudioSound.SetActive(true);
    }

    public void Reload()
    {
        //if you have bullets to reload with
        if (ammoCount > 0)
        {
            //reload
            if (reloadTimer <= 0)
            {
                reloadTimer = reloadSpeed;
                ammoCount -= magSize;
                if (bulletsInMag > 0)
                    ammoCount += bulletsInMag;

                bulletsInMag = magSize;

                if (ammoCount < 0)
                {
                    bulletsInMag += ammoCount;
                    ammoCount -= ammoCount;
                }
                reloading = false;
            }
            reloadTimer -= Time.deltaTime;
        }
        else
        {
            //display message here telling them to find a new weapon
            reloading = false;
        }
    }

    public void StartSpeedKillTimer()
    {
        speedKillStarted = true;
    }

    public GameObject CreateBulletHole(Quaternion hitRotation, Vector3 position)
    {
        if (transform.root.GetComponent<Player_NetworkManager>().isLocalPlayer)
        {
            GameObject newBulletHole = Instantiate(bulletHoleObject, position, hitRotation) as GameObject;
            newBulletHole.hideFlags = HideFlags.HideInHierarchy;
            newBulletHole.transform.position += newBulletHole.transform.forward * -0.005f;
            newBulletHole.GetComponent<SpriteRenderer>().sprite = bulletHoles[0];
            Destroy(newBulletHole, 20);
            if (sparks != null)
                return CreateSparks(hitRotation, position);
            else
                return null;
        }
        else
            return null;
    }

    public GameObject CreateSparks(Quaternion hitRotation, Vector3 position)
    {
        sparks.GetComponent<GIF_Script>().destroyOnEnd = true;
        GameObject spark = Instantiate(sparks, position, hitRotation) as GameObject;
        spark.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));
        spark.transform.position += spark.transform.forward * -0.001f;
        Destroy(spark, 10);
        return spark;
    }

    public Camera FindActiveCamera()
    {
        Camera[] cameras = FindObjectsOfType<Camera>();
        for (int i = 0; i < cameras.Length; ++i)
        {
            if (cameras[i].enabled == true && cameras[i].tag == "PlayerCamera")
                return cameras[i];
        }
        return null;
    }

    public GameObject SpawnEnemyHitParticles(RaycastHit hitInfo)
    {
        var hitRotation = Quaternion.LookRotation(-hitInfo.normal);
        ParticleSystem newParticles = Instantiate(enemyHitEffect, hitInfo.point, hitRotation) as ParticleSystem;
        newParticles.hideFlags = HideFlags.HideInHierarchy;
        Destroy(newParticles.gameObject, 1);
        bodyHitSound.Play();
        return newParticles.gameObject;

    }

    public GameObject SpawnWallHitParticles(RaycastHit hitInfo)
    {
        var hitRotation = Quaternion.LookRotation(-hitInfo.normal);
        ParticleSystem newParticles = Instantiate(wallHitEffect, hitInfo.point, hitRotation) as ParticleSystem;
        newParticles.hideFlags = HideFlags.HideInHierarchy;
        Destroy(newParticles.gameObject, 1);
        return newParticles.gameObject;
    }

    public void HitMarker()
    {
        passedInHitmarker.enabled = true;
        hitmarkerTimer = 0.1f;
    }

    public void ResetMaterial()
    {
        activeMaterial = defaultMaterial;
        materialInvokeCalled = false;
        RimPower = 0;
    }
    #endregion
}
