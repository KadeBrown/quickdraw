﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Quad_Shot_Script : Gun_Script
{
    #region Variables
    [Header("Bullet Settings")]
    public float chargeDelay;        //Cant go higher than 
    private float chargeTimer;
    private bool charging;
    public int pelletsPerShot;

    private bool killedOneThisShell;
    private int temp;
    private bool ableToShoot;

    public AudioSource chargingSound;

    public GameObject electric  = null;
    public GameObject electric1 = null;
    public GameObject electric2 = null;
    public GameObject MuzzleFlash = null;
    public GameObject MuzzleFlash1 = null;
    public GameObject MuzzleFlash2= null;
    public GameObject MuzzleFlash3 = null;
    #endregion

    #region Functions
    // Use this for initialization
    override public void Start()
    {
        defaultMaterial = transform.GetChild(0).GetComponent<Renderer>().material;
        transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().enabled = false;
        playerAnimator = transform.root.GetComponent<Player_ShootManager>().quadAnims.GetComponent<Animator>();

        base.Start();
    }

    // Update is called once per frame
    override public void Update()
    {
        if (active)
        {
            //Hit Marker 
            hitmarkerTimer -= Time.deltaTime;
            if (hitmarkerTimer <= 0)
            {
                passedInHitmarker.enabled = false;
            }

            transform.GetChild(0).GetComponent<Renderer>().enabled = true;

            #region Aiming Down Sights Manipulator
            if (Input.GetMouseButton(1))
                bulletSpread = ADS_Spread;
            else
                bulletSpread = initbulletSpread;
            #endregion

            #region Shooting ( Checking to see if can Shoot)
            //If the player can shoot and the player has pressed the shoot button
            if (Input.GetMouseButton(0) && !reloading && bulletsInMag > 0 && shootTimer <= 0)
            {
                chargeTimer += Time.deltaTime;
                charging = true;
                if (!chargingSound.isPlaying)
                {
                    chargingSound.Play();
                }
            }
            else
            {
                charging = false;
            }

            if(charging)
            {
                activePlayerCamera.GetComponent<Screen_Shake>().ShakeCamera();

                if (electric != null)
                {
                    electric.SetActive(true);

                    electric.GetComponent<GIF_Script>().play = true;
                }

                if (electric1 != null)
                {
                    electric1.SetActive(true);

                    electric1.GetComponent<GIF_Script>().play = true;
                }

                if (electric2 != null)
                {
                    electric2.SetActive(true);
                    electric2.GetComponent<GIF_Script>().play = true;
                }
            }
            else
            {
                if (electric != null)
                {
                    electric.SetActive(false);

                }

                if (electric1 != null)
                {
                    electric1.SetActive(false);

                }

                if (electric2 != null)
                {
                    electric2.SetActive(false);
                }
            }

            if (Input.GetMouseButtonUp(0) || chargeTimer >= bulletsInMag || chargeTimer >= chargeDelay)
            {
                if (shootTimer <= 0 && !reloading && bulletsInMag > 0)
                {
                    charging = false;
                    chargingSound.Stop();

                    int bulletsToShoot = (int)chargeTimer * 2;

                    if (bulletsToShoot > bulletsInMag)
                        bulletsToShoot = bulletsInMag;

                    if (bulletsToShoot < 1)
                        bulletsToShoot = 1;

                    killedOneThisShell = false;
                    for (int i = 0; i < bulletsToShoot; i++)
                    {
                        bulletsInMag -= 1;
                        for (int j = 0; j < pelletsPerShot; j++)
                        {
                            Shoot();
                        }
                    }
                    shootTimer = fireSpeed;
                }
                chargeTimer = 0;

                bulletDamage = initBulletDamage;
            }

            if (charging && shootTimer <= 0)
            {
                ableToShoot = true;
            }
            else
            {
                ableToShoot = false;
            }
            playerAnimator.SetBool("isCharging", ableToShoot);

            if (shootTimer > 0)
                shootTimer -= Time.deltaTime;
            #endregion

            #region Reloading
            if (bulletsInMag <= 0)
                transform.root.GetComponent<Player_Health>().canvasInstance.transform.FindChild("InteractText").GetComponent<Text>().text = "Press 'R' to Reload";
            if (bulletsInMag <= 0 && Input.GetMouseButtonDown(0))
            {
                reloadSound.Play();
                reloading = true;
                playerAnimator.SetTrigger("reload");

            }
            else if (bulletsInMag < magSize && Input.GetKeyDown(KeyCode.R))
            {
                playerAnimator.SetTrigger("reload");
                reloadSound.Play();
                reloading = true;
                if (reloadSound != null && reloading == true && !reloadSound.isPlaying)
                    reloadSound.Play();
            }

            gunAnimator.SetBool("isReloading", reloading);

            if (reloading)
            {
                Reload();

            }
            #endregion

        }
        else
        {
            transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
    }

    override public void Shoot()
    {
        charging = false;
        PlaySound();
        if (MuzzleFlash != null)
            MuzzleFlash.GetComponent<GIF_Script>().play = true;
        if (MuzzleFlash1 != null)
            MuzzleFlash1.GetComponent<GIF_Script>().play = true;
        if (MuzzleFlash2 != null)
            MuzzleFlash2.GetComponent<GIF_Script>().play = true;
        if (MuzzleFlash3 != null)
            MuzzleFlash3.GetComponent<GIF_Script>().play = true;

        activePlayerCamera.GetComponent<Screen_Shake>().ShakeCamera();
        //verticleMuzzleFlash.GetComponent<GIF_Script>().play = true;
        //horizontalMuzzleFlash.GetComponent<GIF_Script>().play = true;
        //GameObject muzzleParent = verticleMuzzleFlash.transform.parent.gameObject;
        //muzzleParent.transform.forward = muzzleParent.transform.parent.transform.forward;
        //muzzleParent.transform.Rotate(new Vector3(0, 0, 1), Random.Range(0, 30));

        outerReticle.transform.localScale *= 1.05f;



        //Makes random radius with given bulletspread ( In meters )
        float randomRadius = Random.Range(0, bulletSpread);
        //Makes random angle from player
        float randomAngle = Random.Range(0, 2 * Mathf.PI);

        //Calculate raycast direction
        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle), randomRadius * Mathf.Sin(randomAngle), bulletRange);

        direction = activePlayerCamera.transform.TransformDirection(direction.normalized);

        //Raycast
        Ray r = new Ray(activePlayerCamera.transform.position, direction);
        RaycastHit hit;
        
        //Shoots tracer in the same direction as the bullet
        GameObject enemyPlayer = transform.root.GetComponent<Player_Health>().enemy;
        if (enemyPlayer != null)
        {
            transform.root.GetComponent<Player_NetworkManager>().CmdServerSpawnTracer(enemyPlayer.GetComponent<Player_NetworkManager>().netId, bulletOrigin.transform.position,
                direction, FindActiveCamera().transform.rotation, tracerSpeed);
        }
        ShootTracer(bulletOrigin.transform.position,
            direction, FindActiveCamera().transform.rotation, tracerSpeed);

        if (Physics.Raycast(r, out hit))
        {
            if (hit.collider.gameObject.tag == "Enemy")
            {
                //tell the enemy to take damage depending on the players damage
                hit.collider.gameObject.GetComponent<Enemy_Base>().TakeDamage(bulletDamage);
                //transform.root.GetComponent<Player_NetworkManager>().CmdServerSpawnObject(SpawnEnemyHitParticles(hit));
                HitMarker();
                if (hit.collider.gameObject.GetComponent<Enemy_Base>().health <= 0)
                {
                    if (!killedOneThisShell)
                    {
                        transform.root.GetComponent<Softcore_Objectives>().AddKill();
                        killedOneThisShell = true;
                        if (!speedKillStarted)
                        {
                            StartSpeedKillTimer();
                            transform.root.GetComponent<Softcore_Objectives>().AddSpeedKill();
                        }
                        else
                        {
                            transform.root.GetComponent<Softcore_Objectives>().AddSpeedKill();
                        }
                    }
                }
            }
            else if (hit.collider.gameObject.tag == "EnemyHead")
            {
                hit.collider.transform.parent.GetComponent<Enemy_Base>().Headshot(bulletDamage);
                if (hit.collider.transform.parent.GetComponent<Enemy_Base>().health <= 0)
                {
                    if (!killedOneThisShell)
                    {
                        transform.root.GetComponent<Softcore_Objectives>().AddKill();
                        killedOneThisShell = true;
                    }
                    if (!speedKillStarted)
                    {
                        StartSpeedKillTimer();
                        transform.root.GetComponent<Softcore_Objectives>().AddSpeedKill();
                    }
                    else
                    {
                        transform.root.GetComponent<Softcore_Objectives>().AddSpeedKill();
                    }
                }
            }
            else if (hit.collider.gameObject.tag == "Head" && hit.collider.gameObject != this.gameObject)
            {
                transform.root.GetComponent<Player_NetworkManager>().CmdDamagePlayer(hit.collider.transform.root.GetComponent<Player_NetworkManager>().netId, bulletDamage * playerHitMultiplier * 2);
                HitMarker();
            }
            else if (hit.collider.gameObject.tag == "Body" && hit.collider.gameObject != this.gameObject)
            {
                transform.root.GetComponent<Player_NetworkManager>().CmdDamagePlayer(hit.collider.transform.root.GetComponent<Player_NetworkManager>().netId, bulletDamage * playerHitMultiplier);
                HitMarker();
            }
            else
            {
                if (enemyPlayer != null)
                {
                    transform.root.GetComponent<Player_NetworkManager>().CmdServerSpawnWallHitEffects(enemyPlayer.GetComponent<Player_NetworkManager>().netId,
                    Quaternion.LookRotation(-hit.normal), hit.point);
                }
                CreateBulletHole(Quaternion.LookRotation(-hit.normal), hit.point);
            }

        }
    }
    #endregion
}
