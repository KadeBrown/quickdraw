﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Rifle_Script : Gun_Script
{
    #region Variables
    [Header ("Muzzle Flash")]
    public GameObject verticleMuzzleFlash   = null;
    public GameObject horizontalMuzzleFlash = null;
    public AudioSource gunNearlyEmptySound;
    #endregion

    #region Functions

    override public void Start()
    {
        defaultMaterial = transform.GetChild(0).GetComponent<Renderer>().material;
        base.Start();
        playerAnimator = transform.root.GetComponent<Player_ShootManager>().rifleAnims.GetComponent<Animator>();
    }
    // Update is called once per frame
    override public void Update()
    {
        if (active)
        {
            transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().enabled = true;
            transform.GetChild(0).GetComponent<Renderer>().material = activeMaterial;

            if (transform.GetChild(0).GetComponent<Renderer>().material != defaultMaterial)
            {
                RimPower = Mathf.PingPong(Time.time * 5, 5);

                transform.GetChild(0).GetComponent<Renderer>().material.SetFloat("_RimPower", RimPower + 3);
                if(!materialInvokeCalled)
                {
                    Invoke("ResetMaterial", 3);
                    materialInvokeCalled = true;
                }
            }


            //Speed Timer
            if (speedKillAttemptsCurrent >= speedKillAttemptsTotal)
            {
                if (speedKillStarted)
                {
                    speedKillTimer += Time.deltaTime;
                    if (speedKillTimer >= speedKillTimeLimit)
                    {
                        speedKillTimer = 0;
                        speedKillStarted = false;
                        transform.root.GetComponent<Softcore_Objectives>().ResetSpeedKills();
                        speedKillAttemptsCurrent++;
                        //Attempted--
                    }
                }
            }

            hitmarkerTimer -= Time.deltaTime;
            if (hitmarkerTimer <= 0 )
            {
                passedInHitmarker.enabled = false;
            }
            if (isLocalPlayer)
            {
                if (canShoot)
                {

                    #region Aiming Down Sights Manipulator
                    if (Input.GetMouseButton(1))
                    {
                        playerAnimator.SetBool("AimingDownSights", true);
                        if (transform.root.GetComponent<Player_Movement>().isGrounded)
                        {
                            transform.root.GetComponent<Player_Movement>().moveSpeed *= 0.5f;
                        }
                        bulletSpread = ADS_Spread;

                        if (activePlayerCamera.GetComponent<Camera>().fieldOfView > 40)
                            activePlayerCamera.GetComponent<Camera>().fieldOfView -= Time.deltaTime * 150;
                    }
                    else
                    {
                        playerAnimator.SetBool("AimingDownSights", false);
                       // transform.root.GetComponent<Player_Movement>().SpeedUp();
                        bulletSpread = initbulletSpread;
                        if (activePlayerCamera.GetComponent<Camera>().fieldOfView < 59)
                            activePlayerCamera.GetComponent<Camera>().fieldOfView += Time.deltaTime * 200;
                    }
                    #endregion

                    #region Shooting ( Checking to see if can Shoot)
                    //If the player can shoo and the player has pressed the shoot button
                    if (shootTimer <= 0 && Input.GetMouseButton(0) && !reloading && bulletsInMag > 0)
                    {
                        //Shoot and reset the delay
                        for (int i = 0; i < bulletsPerShot; ++i)
                        {
                            Shoot();
                        }

                        shootTimer = fireSpeed;
                    }
                    else if (!Input.GetMouseButton(0))
                    {
                        playerAnimator.SetBool("isShooting", false);
                    }

                    if (shootTimer > 0)
                        shootTimer -= Time.deltaTime;
                    #endregion

                    #region Reloading
                    if (bulletsInMag <= 0)
                        transform.root.GetComponent<Player_Health>().canvasInstance.transform.FindChild("InteractText").GetComponent<Text>().text = "Press 'R' to Reload";
                    if (bulletsInMag <= 0 && Input.GetMouseButtonDown(0))
                    {
                        reloading = true;
                        playerAnimator.SetTrigger("reload");

                    }
                    else if (bulletsInMag < magSize && Input.GetKeyDown(KeyCode.R))
                    {
                        playerAnimator.SetTrigger("reload");

                        reloading = true;
                        if (reloadSound != null && reloading == true && !reloadSound.isPlaying)
                            reloadSound.Play();
                    }
                    

                    gunAnimator.SetBool("isReloading", reloading);
                    playerAnimator.SetBool("isReloading", reloading);
                    // gunAnimator.SetBool("isReloading", reloading);
                    transform.root.GetComponent<Player_NetworkManager>().CmdSyncAnimation(transform.root.GetComponent<Player_NetworkManager>().netId, "Reload", reloading);

                    if (reloading)
                        Reload();

  


                }
                else
                {
                    playerAnimator.SetBool("isShooting", false);
                    reloading = false;
                    gunAnimator.SetBool("isReloading", reloading);
                    playerAnimator.SetBool("isReloading", reloading);
                    transform.root.GetComponent<Player_NetworkManager>().CmdSyncAnimation(transform.root.GetComponent<Player_NetworkManager>().netId, "Reload", reloading);

                    reloadTimer = reloadSpeed;
                    if (reloading == false)
                        reloadSound.Stop();
                }
                #endregion



            }
        }
        else
        {
            playerAnimator.SetBool("isShooting", false);
            reloading = false;
            gunAnimator.SetBool("isReloading", reloading);
            playerAnimator.SetBool("isReloading", reloading);
            transform.root.GetComponent<Player_NetworkManager>().CmdSyncAnimation(transform.root.GetComponent<Player_NetworkManager>().netId, "Reload", reloading);

            reloadTimer = reloadSpeed;
            transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
    }

    override public void Shoot()
    {
        playerAnimator.SetBool("isShooting", true);
        activePlayerCamera.GetComponent<Screen_Shake>().ShakeCamera();
        verticleMuzzleFlash.GetComponent<GIF_Script>().play = true;
        horizontalMuzzleFlash.GetComponent<GIF_Script>().play = true;
        GameObject muzzleParent = verticleMuzzleFlash.transform.parent.gameObject;
        muzzleParent.transform.forward = muzzleParent.transform.parent.transform.forward;
        muzzleParent.transform.Rotate(new Vector3(0,0,1), Random.Range(0,30));

        outerReticle.transform.localScale *= 1.2f;
        //Everytime Shoot is called, one bullet is taken out of the mag
        bulletsInMag -= 1;
        PlaySound();
        if (bulletsInMag <= 5)
        {
            gunNearlyEmptySound.Play();
        }

        //Makes random radius with given bulletspread ( In meters )
        float randomRadius = Random.Range(0, bulletSpread);
        //Makes random angle from player
        float randomAngle = Random.Range(0, 2 * Mathf.PI);

        //Calculate raycast direction
        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle), randomRadius * Mathf.Sin(randomAngle), bulletRange);

        direction = activePlayerCamera.transform.TransformDirection(direction.normalized);

        //Raycast
        Ray r = new Ray(activePlayerCamera.transform.position, direction);
        RaycastHit hit;



        //Shoots tracer in the same direction as the bullet
        GameObject enemyPlayer = transform.root.GetComponent<Player_Health>().enemy;
        if (enemyPlayer != null)
        {
            transform.root.GetComponent<Player_NetworkManager>().CmdServerSpawnTracer(enemyPlayer.GetComponent<Player_NetworkManager>().netId, bulletOrigin.transform.position,
                direction, FindActiveCamera().transform.rotation, tracerSpeed);
        }
        ShootTracer(bulletOrigin.transform.position,
            direction, FindActiveCamera().transform.rotation, tracerSpeed);

        if (Physics.Raycast(r, out hit))
        {

            if (hit.collider.gameObject.tag == "Enemy")
            {
                //tell the enemy to take damage depending on the players damage
                hit.collider.gameObject.GetComponent<Enemy_Base>().TakeDamage(bulletDamage);
                SpawnEnemyHitParticles(hit);
                HitMarker();
                if (hit.collider.gameObject.GetComponent<Enemy_Base>().health <= 0)
                {
                    if (!speedKillStarted)
                    {
                        StartSpeedKillTimer();
                    }
                        transform.root.GetComponent<Softcore_Objectives>().AddSpeedKill();
                }
            }
            else if (hit.collider.gameObject.tag == "EnemyHead")
            {
                hit.collider.transform.parent.GetComponent<Enemy_Base>().Headshot(bulletDamage);
                SpawnEnemyHitParticles(hit);
                if (hit.collider.transform.parent.GetComponent<Enemy_Base>().health <= 0)
                {
                    transform.root.GetComponent<Softcore_Objectives>().AddHeadShot();
                    if (!speedKillStarted)
                    {
                        StartSpeedKillTimer();
                        transform.root.GetComponent<Softcore_Objectives>().AddSpeedKill();
                    }
                    else
                    {
                        transform.root.GetComponent<Softcore_Objectives>().AddSpeedKill();
                    }
                }
            }
            else if (hit.collider.gameObject.tag == "Head" && hit.collider.gameObject != this.gameObject)
            {
                transform.root.GetComponent<Player_NetworkManager>().CmdDamagePlayer(hit.collider.transform.root.GetComponent<Player_NetworkManager>().netId, bulletDamage * playerHitMultiplier * 2);
                HitMarker();
            }
            else if (hit.collider.gameObject.tag == "Body" && hit.collider.gameObject != this.gameObject)
            {

                transform.root.GetComponent<Player_NetworkManager>().CmdDamagePlayer(hit.collider.transform.root.GetComponent<Player_NetworkManager>().netId, bulletDamage * playerHitMultiplier);
                HitMarker();
            }
            else
            {
                if (enemyPlayer != null)
                {
                    transform.root.GetComponent<Player_NetworkManager>().CmdServerSpawnWallHitEffects(enemyPlayer.GetComponent<Player_NetworkManager>().netId,
                    Quaternion.LookRotation(-hit.normal), hit.point);
                }
                CreateBulletHole(Quaternion.LookRotation(-hit.normal), hit.point);
            }
        }
    }
    #endregion
}
    
